<?php
session_start();

// Flash message helper
// EXAMPLE - flash('register_success', 'You are now registered', 'alert alert-danger');
// DISPLAY IN VIEW - <?php echo flash('register_success');
function flash($name='', $message= '', $class = 'alert alert-success') {
	if(!empty($name)) {
		if(!empty($message) && empty($_SESSION[$name])) { // set flash message

			if(!empty($_SESSION[$name])) {
				unset($_SESSION[$name]);
			}

			if(!empty($_SESSION[$name . '_class'])) {
				unset($_SESSION[$name . '_class']);
			}

			$_SESSION[$name] = $message;
			$_SESSION[$name . '_class'] = $class;
		}
		else if(empty($message) && !empty($_SESSION[$name])) { // display flash message

			$class = !empty($_SESSION[$name . '_class']) ? $_SESSION[$name . '_class']: '';
			echo '<div class="'.$class.'" id="msg-flash">'.$_SESSION[$name].'</div>';
			unset($_SESSION[$name]);
			unset($_SESSION[$name . '_class']);
		}
	}
}


function isLoggedIn() {
	if($_SESSION['user_id']) {
		return true;
	}
	return false;
}

// $_SESSION['user'] = 'TEST';

// unset($_SESSION['user']);

