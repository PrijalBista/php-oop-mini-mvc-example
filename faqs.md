### Some FAQs

- spl_autoload_register
https://stackoverflow.com/questions/7651509/what-is-autoloading-how-do-you-use-spl-autoload-autoload-and-spl-autoload-re


- call_user_func_array
https://stackoverflow.com/questions/18526060/why-should-one-prefer-call-user-func-array-over-regular-calling-of-function

- Installation
	- git clone repo
	- move it to htdocs or www folder /htdocs/mymvc
	- Set the config variables in config/config.php
		Example-
		define('URL_ROOT', 'http://127.0.0.1/mymvc')
	- Set the RewriteBase in mymvc/public/.htaccess file. (the one located inside public directory only)
	- Your Done !

